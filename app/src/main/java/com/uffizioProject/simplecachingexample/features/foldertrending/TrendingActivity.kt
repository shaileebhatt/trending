package com.uffizioProject.simplecachingexample.features.foldertrending

import android.app.Activity
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.PopupMenu
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.uffizioProject.simplecachingexample.R
import com.uffizioProject.simplecachingexample.databinding.ActivityTrendingBinding
import com.uffizioProject.simplecachingexample.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_trending.*
import kotlinx.android.synthetic.main.lay_action_bar.*
import kotlinx.android.synthetic.main.lay_error.*


//to begin working with Dagger we need to annotate the application class with @HiltAndroidApp
@AndroidEntryPoint
class TrendingActivity : AppCompatActivity() {
    private val viewModel: TrendingViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trending)
        val binding = ActivityTrendingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val trendingAdapter = TrendingAdapter()

        binding.apply {
            recyclerView.apply {
                adapter = trendingAdapter
                layoutManager = LinearLayoutManager(this@TrendingActivity)
            }

            viewModel.trendingData.observe(this@TrendingActivity) { result ->
                trendingAdapter.submitList(result.data)
                shimmerFrameLayout.stopShimmerAnimation()
                shimmerFrameLayout.visibility = View.GONE
                recycler_view.visibility = View.VISIBLE
                progressBar.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
                textViewError.isVisible = result is Resource.Error && result.data.isNullOrEmpty()
                textViewError.text = result.error?.localizedMessage
             //   layError.isVisible = result is Resource.Error && result.data.isNullOrEmpty()
            }
        }
        swipe.setOnRefreshListener {
            if (isNetworkAvailable()){
                swipe.isRefreshing = true
                lay_error_include.visibility=View.GONE
                rlDataLayout.visibility=View.VISIBLE
                binding.apply {
                    recyclerView.apply {
                        adapter = trendingAdapter
                        layoutManager = LinearLayoutManager(this@TrendingActivity)
                    }

                    viewModel.trendingData.observe(this@TrendingActivity) { result ->
                        trendingAdapter.submitList(result.data)
                        shimmerFrameLayout.stopShimmerAnimation()
                        shimmerFrameLayout.visibility = View.GONE
                        recycler_view.visibility = View.VISIBLE
                        progressBar.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
                        textViewError.isVisible = result is Resource.Error && result.data.isNullOrEmpty()
                        textViewError.text = result.error?.localizedMessage
                    }
                }
                swipe.isRefreshing = false
            }
            else if (!isNetworkAvailable()){
                swipe.isRefreshing = true
                rlDataLayout.visibility=View.GONE
                lay_error_include.visibility=View.VISIBLE
                swipe.isRefreshing = false
            }
        }
        btnRetry.setOnClickListener {
            Log.e("btnClicked","btnRetry")
            if (isNetworkAvailable()){
                swipe.isRefreshing = true
                lay_error_include.visibility=View.GONE
                rlDataLayout.visibility=View.VISIBLE
                binding.apply {
                    recyclerView.apply {
                        adapter = trendingAdapter
                        layoutManager = LinearLayoutManager(this@TrendingActivity)
                    }

                    viewModel.trendingData.observe(this@TrendingActivity) { result ->
                        trendingAdapter.submitList(result.data)
                        shimmerFrameLayout.stopShimmerAnimation()
                        shimmerFrameLayout.visibility = View.GONE
                        recycler_view.visibility = View.VISIBLE
                        progressBar.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
                        textViewError.isVisible = result is Resource.Error && result.data.isNullOrEmpty()
                        textViewError.text = result.error?.localizedMessage
                    }
                }
                swipe.isRefreshing = false
            }
        }
        btnMenu.setOnClickListener{
            showMenu(btnMenu)
        }
        if(isDarkTheme(this)){
            btnMenu.setImageResource(R.drawable.imagemenu)
        }else{
            btnMenu.setImageResource(R.drawable.menu_icon)
        }
    }
    override fun onResume() {
        super.onResume()
        shimmerFrameLayout.startShimmerAnimation()
    }

    override fun onPause() {
        shimmerFrameLayout.stopShimmerAnimation()
        super.onPause()
    }
    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu,menu)
        return true
    }
    fun showMenu(v: View?) {
        val popup = PopupMenu(this, v)
        // This activity implements OnMenuItemClickListener
       // popup.setOnMenuItemClickListener(this)
        popup.inflate(R.menu.menu)
        popup.show()
    }
    fun isDarkTheme(activity: Activity): Boolean {
        return activity.resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
    }
}