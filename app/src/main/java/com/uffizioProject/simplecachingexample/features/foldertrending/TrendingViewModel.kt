package com.uffizioProject.simplecachingexample.features.foldertrending

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.uffizioProject.simplecachingexample.data.TrendingRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TrendingViewModel @Inject constructor(
    repository: TrendingRepository
) : ViewModel() {

    val trendingData = repository.getTrending().asLiveData()
    val authorReceived = MutableLiveData<String>()
    val nameReceived = MutableLiveData<String>()
    val desReceived = MutableLiveData<String>()
    val languageReceived = MutableLiveData<String>()
    val starsReceived = MutableLiveData<Int>().toString()
    val forkReceived = MutableLiveData<Int>().toString()
}