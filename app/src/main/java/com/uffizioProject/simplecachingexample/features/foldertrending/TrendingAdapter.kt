package com.uffizioProject.simplecachingexample.features.foldertrending

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.uffizioProject.simplecachingexample.data.TrendingModel.TrendingModelItem
import com.uffizioProject.simplecachingexample.databinding.TrendingItemBinding

class TrendingAdapter :
    ListAdapter<TrendingModelItem, TrendingAdapter.TrendingViewHolder>(RestaurantComparator()) {
    private var expandedItemIndex = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrendingViewHolder {
        val binding =
            TrendingItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TrendingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TrendingViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

   inner class TrendingViewHolder(private val binding: TrendingItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(tr: TrendingModelItem) {
            binding.apply {
                Glide.with(itemView)
                    .load(tr.avatar)
                    .into(ivAvatar)
                tvName.text = tr.name
                tvAuthor.text = tr.author
                tvDescription.text = tr.description
                tvLanguage.text = tr.language
                tvCurrentPeriodStars.text = tr.currentPeriodStars.toString()
                tvForks.text = tr.forks.toString()
                itemView.setOnClickListener {
                    if (collapseConstraint.visibility == View.GONE) {
                        Log.e("collapseConstarint","Gone")
                        collapseConstraint.visibility = View.VISIBLE
                    } else {
                        Log.e("collapseConstarint","Visible")
                        collapseConstraint.visibility = View.GONE
                    }
                }
                itemView.setOnClickListener { v ->
                    if (position === expandedItemIndex) {
                        notifyItemChanged(position)
                        expandedItemIndex = -1
                    } else {
                        if (expandedItemIndex != -1) {
                            notifyItemChanged(expandedItemIndex)
                        }
                        expandedItemIndex = position
                        notifyItemChanged(position)
                    }
                }
                if (position == expandedItemIndex) {
                    // Expand
                    collapseConstraint.visibility = View.VISIBLE
                } else {
                    // Collapse
                    collapseConstraint.visibility = View.GONE
                }
            }
        }
    }

    class RestaurantComparator : DiffUtil.ItemCallback<TrendingModelItem>() {
        override fun areItemsTheSame(oldItem: TrendingModelItem, newItem: TrendingModelItem) =
            oldItem.name == newItem.name

        override fun areContentsTheSame(oldItem: TrendingModelItem, newItem: TrendingModelItem) =
            oldItem == newItem
    }

}