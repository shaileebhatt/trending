package com.uffizioProject.simplecachingexample.data

import androidx.room.withTransaction
import com.uffizioProject.simplecachingexample.api.TrendingApi
import com.uffizioProject.simplecachingexample.util.networkBoundResource
import kotlinx.coroutines.delay
import javax.inject.Inject


//here , @Inject is helping in passing the dependency required in constructor itselff
class TrendingRepository @Inject constructor(
    private val api: TrendingApi,
    private val db: TrendingDatabase
) {
    private val trendingDao = db.trendingDao()

    fun getTrending() = networkBoundResource(
        query = {
            trendingDao.getAllTrending()
        },
        fetch = {
            delay(2000)
            api.getTrending()
        },
        saveFetchResult = { restaurants ->
            db.withTransaction {
                trendingDao.deleteAllTrending()
                trendingDao.insertTrending(restaurants)
            }
        }
    )
}