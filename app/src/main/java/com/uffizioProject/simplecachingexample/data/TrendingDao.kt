package com.uffizioProject.simplecachingexample.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface TrendingDao {

    @Query("SELECT * FROM trending")
    fun getAllTrending(): Flow<List<TrendingModel.TrendingModelItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTrending(restaurants: List<TrendingModel.TrendingModelItem>)

    @Query("DELETE FROM trending")
    suspend fun deleteAllTrending()
}