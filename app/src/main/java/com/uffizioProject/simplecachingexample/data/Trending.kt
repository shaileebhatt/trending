package com.uffizioProject.simplecachingexample.data

import androidx.room.Entity
import androidx.room.PrimaryKey
class TrendingModel : ArrayList<TrendingModel.TrendingModelItem>(){
    @Entity(tableName = "trending")
    data class TrendingModelItem(
        val author: String,
        val avatar: String,
        val currentPeriodStars: Int,
        val description: String,
        val forks: Int,
        val language: String,
        val languageColor: String,
        @PrimaryKey val name: String,
        val stars: Int,
        val url: String
    )
}