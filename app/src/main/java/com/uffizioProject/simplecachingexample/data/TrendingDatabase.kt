package com.uffizioProject.simplecachingexample.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [TrendingModel.TrendingModelItem::class], version = 1)
abstract class TrendingDatabase : RoomDatabase() {

    abstract fun trendingDao(): TrendingDao
}