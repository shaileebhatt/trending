package com.uffizioProject.simplecachingexample

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TrendingApplication : Application()