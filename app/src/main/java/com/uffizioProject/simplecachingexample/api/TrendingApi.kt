package com.uffizioProject.simplecachingexample.api

import com.uffizioProject.simplecachingexample.data.TrendingModel
import retrofit2.http.GET

interface TrendingApi {

    companion object {
       const val BASE_URL = "https://private-2e3ad-githubtrendingapi.apiary-mock.com/"
    }
    @GET("repositories")
    suspend fun getTrending(): List<TrendingModel.TrendingModelItem>
}
