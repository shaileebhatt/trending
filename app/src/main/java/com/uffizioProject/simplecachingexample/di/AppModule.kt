package com.uffizioProject.simplecachingexample.di

import android.app.Application
import androidx.room.Room
import com.uffizioProject.simplecachingexample.api.TrendingApi
import com.uffizioProject.simplecachingexample.data.TrendingDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

//module as a provider of dependency and is a class
//A module is installed in a Hilt Component by annotating the module with the @InstallIn annotation.

// to provide dependency,Component works as that specific bridge (it is an interface)  //outdated

//Provides are annotation which is used in Module class to provide dependency

//Inject is an annotation that is used to define a dependency inside the consumer.

//  InstallIn :  This means that the dependencies provided here will be used across the application
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(TrendingApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideRestaurantApi(retrofit: Retrofit): TrendingApi =
        retrofit.create(TrendingApi::class.java)

    @Provides
    @Singleton
    fun provideDatabase(app: Application) : TrendingDatabase =
        Room.databaseBuilder(app, TrendingDatabase::class.java, "sample_database")
            .build()
}